const express = require ('express');
// create an application using express
// this creates an express application and stores this as a constant called app. 
//app is our server
const app = express()
//for our application server to run, we need a port to listen to.
const port = 3000;

//Setup for allowing the server to handle data from requests
//Allows your app to read json data
//Methods used from express JS are middlewares
//Middleware is a software that provides services outside of what's offered by the operating system 
app.use(express.json());
//allows your app to road data from forms
//Be default, information received from the url can only be received as a string or an array
//By applying the option of "extended:true" this allows us to receive information in other data types such as an object/boolean etc, which we will use throught our application
app.use(express.urlencoded({ extended: true}))



//We will create routes
//Express has methods correspoonding to each HTTP method
//This route expects to receive a GET request at the base URI '/'
//"http://localhost:3000/"
app.get('/', (req,res) =>{
	//res.send uses the express JS module's method instead to send a response back to the client
	res.send("Hello World")
})

app.get('/home', (req,res) =>{
	res.send("Hallo from moi")
})

//Post method
app.post('/hello', (req,res) =>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

//for /signup
//mock database
let users = [];

app.get('/users', (req,res)=>{
	res.send(users)
	console.log(users)
})

app.post('/signup', (req,res) => {
	// console.log(req.body)
	//if contents of the request body with the property "username" and "password" is not empty
	if(req.body.username !== '' && req.body.password !== ''){
		//This will store the user object sent via POSTMAN to the users array created above
		console.log(req.body)
		users.push(req.body)
		console.log(users)
		//send response
		res.send(`User ${req.body.username} successfully registered!`)
	}else{
		res.send("Please input BOTH username and password")
	}
})

app.delete('/delete-user', (req,res)=>{
	users.splice(0,1)
	res.send(users)
	console.log(users)
})


//update the password of a user that matches the information provided in the client /postman
app.listen(port, () => console.log(`Server is running at port ${port}`))
